/* Copyright 2023 ACES-RG */
#include <stdio.h>

#include <pico/stdlib.h>

#include <FreeRTOS.h>
#include <task.h>

#include <lib/driver/uart.h>
#include <lib/sensor/tfminis.h>

static DEFINE_UART(uart_tfmini, 1, 115200, 8, 1, UART_PARITY_NONE, 8, 9, tfminis_uart1_rx_isr);
static DEFINE_TFMINIS(tfmini, &uart_tfmini);

void tfmini_forward_to_usb(void *pvParameters) {
    stdio_init_all();
    uart_tfmini.init(&uart_tfmini);
    vTaskDelay(1000 / portTICK_PERIOD_MS);
    for (;;) {
        tfmini.update(&tfmini);
        printf("\n\n\n\n\n\n\n\n\n\n");
        printf("dist: %d\nstrenght: %d\ntemp: %f\n", tfmini.data.dist_cm, tfmini.data.strenght, tfmini.data.temp_c);
        vTaskDelay(100 / portTICK_PERIOD_MS);
    }
}

int main() {
    xTaskCreate(&tfmini_forward_to_usb, "TFMINIS to usb", 256, NULL, 2, NULL);
    vTaskStartScheduler();
    while (1) {
        // hi
    }
}

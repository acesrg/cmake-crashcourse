/* Copyright 2023 ACES-RG */
#ifndef SRC_LIB_COMMON_FRAME_H_
#define SRC_LIB_COMMON_FRAME_H_

typedef struct {
    uint8_t *buf;
    size_t size;
    size_t pos;
} frame_t;

#define DECLARE_FRAME(__buf, __size) {  \
    .buf = (uint8_t *)(__buf),          \
    .size = (__size),                   \
    .pos = 0,                           \
}

#define DECLARE_FRAME_SPACE(__buf_size) DECLARE_FRAME((uint8_t[__buf_size]){}, __buf_size)

#endif  // SRC_LIB_COMMON_FRAME_H_

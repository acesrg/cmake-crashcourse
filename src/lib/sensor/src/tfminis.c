/* Copyright 2023 ACES-RG */
#include <lib/common/retval.h>
#include <lib/sensor/tfminis.h>
#include <lib/driver/uart.h>
#define TFMINI_UART_HEADER 0x59
#define TFMINI_TEMPERATURE(x) ((((float)x)/8.0f) - 256.0f)

/*
 * |  0  |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |
 * |  HEADER   |DIST |DIST |STREN|STREN|TEMP |TEMP |CHECK|
 * |0x59 | 0x59|LOW  |HIGH |LOW  |HIGH |LOW  |HIGH |     |
 **/
typedef struct {
    uint8_t header_0;
    uint8_t header_1;
    uint8_t dist_low;
    uint8_t dist_high;
    uint8_t strenght_low;
    uint8_t strenght_high;
    uint8_t temp_low;
    uint8_t temp_high;
    uint8_t checksum;
} tfminis_uart_packet_t;

/*
 * A maximum of 2 packets are supported by this lib, if more
 * devices are used more packets should be added.
 *
 * The packets are used by the interrupt to dump the uart data.
 **/
static tfminis_uart_packet_t *pkt0;
static tfminis_uart_packet_t *pkt1;

static frame_t tfminis_recv_frame = DECLARE_FRAME_SPACE(9);
void tfminis_uart0_rx_isr(void) {
    uart0_recv_from_isr(&tfminis_recv_frame);
    pkt0 = (tfminis_uart_packet_t *) tfminis_recv_frame.buf;
}

void tfminis_uart1_rx_isr(void) {
    uart1_recv_from_isr(&tfminis_recv_frame);
    pkt1 = (tfminis_uart_packet_t *) tfminis_recv_frame.buf;
}

static retval_t calculate_tfminis_checksum(tfminis_uart_packet_t *pkt) {
    uint16_t sum = pkt->header_0 + pkt->header_1 + \
                   pkt->dist_high + pkt->dist_low + \
                   pkt->strenght_high + pkt->strenght_low + \
                   pkt->temp_high + pkt->temp_low;
    if ((uint8_t) sum != pkt->checksum) {
        return RV_ERROR;
    }
    return RV_SUCCESS;
}

static retval_t decode_serial_packets(tfminis_t *tfmini) {
    tfminis_uart_packet_t *pkt;
    if (tfmini->uart->config.id == 0) {
        *pkt = *pkt0;
    } else if (tfmini->uart->config.id == 1) {
        *pkt = *pkt1;
    }

    if (TFMINI_UART_HEADER != pkt->header_0 || TFMINI_UART_HEADER != pkt->header_1) {
        return RV_ERROR;
    }

    SUCCESS_OR_RETURN(calculate_tfminis_checksum(pkt));

    tfmini->data.dist_cm = (pkt->dist_high << 8) + pkt->dist_low;
    tfmini->data.strenght = (pkt->strenght_high << 8) + pkt->strenght_low;
    tfmini->data.temp_c = TFMINI_TEMPERATURE((pkt->temp_high << 8) + pkt->temp_low);
    return RV_SUCCESS;
}

retval_t tfminis_update(tfminis_t *tfmini) {
    return decode_serial_packets(tfmini);
}

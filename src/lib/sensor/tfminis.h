/* Copyright 2023 ACES-RG */
#ifndef SRC_LIB_SENSOR_TFMINIS_H_
#define SRC_LIB_SENSOR_TFMINIS_H_
#include <stdint.h>
#include <lib/driver/uart.h>


typedef struct {
    int16_t dist_cm;
    uint16_t strenght;
    float temp_c;
} tfminis_data_t;

typedef struct tfminis_s tfminis_t;

typedef retval_t (tfmini_update_t)(tfminis_t *tfmini);

struct tfminis_s {
    tfminis_data_t data;
    uart_t *uart;
    tfmini_update_t *update;
};

retval_t tfminis_update(tfminis_t *tfmini);
void tfminis_uart0_rx_isr(void);
void tfminis_uart1_rx_isr(void);


#define _DECLARE_TFMINIS_DEFAULT_DATA() {   \
    .dist_cm = -1,                          \
    .strenght = 0,                          \
    .temp_c =   0,                          \
}

#define DEFINE_TFMINIS(_name, _uart)                    \
    tfminis_t _name = {                                 \
        .data = _DECLARE_TFMINIS_DEFAULT_DATA(),        \
        .uart = _uart,                                  \
        .update = tfminis_update,                       \
    }

#endif  // SRC_LIB_SENSOR_TFMINIS_H_

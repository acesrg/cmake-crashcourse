/* Copyright 2023 ACES-RG */
#include <lib/driver/uart.h>
#include "hardware/irq.h"
#include "pico/stdlib.h"

retval_t uart_dev_init(uart_t *uart) {
    if (NULL == uart) {
        return RV_ILLEGAL;
    }
    uart->config.instance = (uart_inst_t *) uart_get_instance(uart->config.id);
    uart_inst_t *instance = uart_get_instance(uart->config.id);

    uart_init(uart->config.instance, uart->config.baud_rate);

    gpio_set_function(uart->config.tx_pin, GPIO_FUNC_UART);
    gpio_set_function(uart->config.rx_pin, GPIO_FUNC_UART);

    uart_set_hw_flow(uart->config.instance, false, false);

    uart_set_format(uart->config.instance, uart->config.data_bits, uart->config.stop_bits, uart->config.parity);

    if (NULL != uart->config.irq) {
        int UART_IRQ = uart->config.id == 0 ? UART0_IRQ : UART1_IRQ;
        irq_set_exclusive_handler(UART_IRQ, uart->config.irq);
        irq_set_enabled(UART_IRQ, true);
        uart_set_irq_enables(uart->config.instance, true, false);
    }

    return RV_SUCCESS;
}

retval_t uart_send(uart_t *uart, frame_t *send_frame) {
    return RV_NOTIMPLEMENTED;
}

retval_t uart_recv(uart_t *uart, frame_t *recv_frame) {
    if (uart_is_readable(uart->config.instance)) {
        uart_read_blocking(uart->config.instance, recv_frame->buf, recv_frame->size);
    }
}

static retval_t uart_flush(uart_inst_t *uart) {
    while (uart_is_readable(uart)) {
        uart_getc(uart);
    }
}

retval_t uart0_recv_from_isr(frame_t *recv_frame) {
    if (uart_is_readable(uart0)) {
        uart_read_blocking(uart0, recv_frame->buf, recv_frame->size);
        uart_flush(uart0);
    }
}

retval_t uart1_recv_from_isr(frame_t *recv_frame) {
    if (uart_is_readable(uart1)) {
        uart_read_blocking(uart1, recv_frame->buf, recv_frame->size);
        uart_flush(uart1);
    }
}

